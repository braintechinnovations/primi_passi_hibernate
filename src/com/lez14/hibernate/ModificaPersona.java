package com.lez14.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.lez14.hibernate.model.Persona;

	public class ModificaPersona {
	
		public static void main(String[] args) {
	
			SessionFactory factory = new Configuration()
					.configure("/resources/hibernate_persona.cfg.xml")
					.addAnnotatedClass(Persona.class)			//Classe che devo mappare sul DB che possiede delle annotazioni
					.buildSessionFactory();
			
			Session sessione =  factory.getCurrentSession();
			
			try {
				
				sessione.beginTransaction();
				
				Persona gio = sessione.get(Persona.class, 1);

				gio.setNome("Soul");
				gio.setCognome("Goodman");
				
				sessione.save(gio);
				
				sessione.getTransaction().commit();
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				sessione.close();
				System.out.println("Connessione chiusa");
			}
			
		}
}
