package com.lez14.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.lez14.hibernate.model.Persona;

public class RicercaPersona {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration()
				.configure("/resources/hibernate_persona.cfg.xml")
				.addAnnotatedClass(Persona.class)			//Classe che devo mappare sul DB che possiede delle annotazioni
				.buildSessionFactory();
		
		Session sessione =  factory.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
//			List<Persona> elenco = sessione.createQuery("FROM Persona").list();	//SELECT * FROM Persona
			List<Persona> elenco = sessione.createQuery("FROM Persona WHERE email = 'giovanni@ciao.com'").list();	//SELECT * FROM Persona
			
			for(int i=0; i<elenco.size(); i++) {
				Persona temp = elenco.get(i);
				System.out.println(temp.toString());
			}
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Connessione chiusa");
		}
		
	}
	
}
