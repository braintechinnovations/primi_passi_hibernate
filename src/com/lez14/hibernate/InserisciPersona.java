package com.lez14.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.lez14.hibernate.model.Persona;

public class InserisciPersona {

	public static void main(String[] args) {

		/*
		 * Creo la Session Factory (fabbrica di sessioni) per generare una nuova sessione a partire
		 * da un file di configurazione e dalle annotated class che specifico
		 */
		SessionFactory factory = new Configuration()
				.configure("/resources/hibernate_persona.cfg.xml")
				.addAnnotatedClass(Persona.class)			//Classe che devo mappare sul DB che possiede delle annotazioni
				.buildSessionFactory();
		
		/*
		 * Prendo la sessione generata dalla Session Factory e la utilizzo (nelle righe successive) 
		 * per effettuare le operazioni, si tratta del nostro tunnel di comunicazione con il DB
		 */
		Session sessione = factory.getCurrentSession();
		
		Persona perUno = new Persona("Giovanni", "Pace", "giovanni@ciao.com");
		Persona perDue = new Persona("Mario", "Rossi", "mario@ciao.com");
		Persona perTre = new Persona("Valeria", "Verdi", "valeria@ciao.com");
		
		try {
			
			sessione.beginTransaction();			//1. Avvio una nuova transazione
			
			sessione.save(perUno);					//2. Effettuo tutte le operazioni che voglio
			sessione.save(perDue);
			sessione.save(perTre);
			
			sessione.getTransaction().commit();		//3. Se tutte le operazioni vanno a buon fine allora effettuo la Commit! Altrimenti, Rollback!
			
			System.out.println(perUno.toString());
			System.out.println(perDue.toString());
			System.out.println(perTre.toString());
			
		} catch (Exception errore) {
			System.out.println(errore.getMessage());
		} finally {
			sessione.close();						//Alla fine dell'utilizzo della sessione, chiudo!
			System.out.println("Connessione Chiusa!");
		}
		
		
	}

}
